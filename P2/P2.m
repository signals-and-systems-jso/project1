clc
clear
close all

fun = input ('please enter your signal f(x) :' , 's');
fun = ['@(x)' , fun ];
fun = eval(fun);
x = -10:1:10 ;

z=energy(fun(x))

% first parts functions energy
ddirac = @(x) double(not(x));
q=@(x) 4 * ddirac(2*x) - ddirac(x + 3) + 3 * ddirac(x - 1);
s=@(x) q(2 * x);
p=@(x) q(-x+3);
y=@(x) 2*cos(.45*x) .* exp(-.6*(1i)*x) .* (heaviside(x)- heaviside(x-15));

Eq=energy(q(x))
Es=energy(s(x))
Ep=energy(p(x))
Ey=energy(y(x))