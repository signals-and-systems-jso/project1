function y=energy(x) 
    y=sum(abs(x) .* abs(x));
end