clc
clear
close all

n= -10:1:10;
X1=@(n) ((1.1).^(2 * n)) .* (heaviside(n+3)- heaviside(n-2));
X2=@(n) (sin(2*n)+exp(1i*pi*n)) .* (heaviside(n+3)- heaviside(n-5));
% we define the second function as a matrix and find the values
x2 = zeros();
for i = 1:6 
    m = -10:i;
    x2(i) = sum(X2(m)); 
end
figure();
subplot(2,1,1);
% my own function for conv
Conv1 = my_conv(X1(n), x2);
stem(Conv1);
title('my_conv');
subplot(2,1,2);
% matlab's function for conv
Conv2 = conv(X1(n), x2);
stem(Conv2);
title('matlab conv');