clc
clear
close all

t = 0:0.01:2;
x = sin (2*pi*t);
y = awgn(x,20);
plot(t,y);