clc
clear
close all

t = 0:0.01:2;
x = sin (2*pi*t);
y = awgn(x,20);
m = ones(1,7) ./ 7;
z = conv(y , m ,'same');
plot(t,z);
hold on
plot(t,x);
hold on
plot(t,y);