clc
clear
close all

t = 0:0.01:2;
t_c = 0:0.01:2.06;
x = sin (2*pi*t);
m = ones(1,7) ./ 7;
convXM = conv(x , m ,'same');
figure();
plot(t,x,'g');
hold on
plot(t,convXM);
% or because of convolve our function goes longer
% convXM = conv(x , m )
% plot(t_c,convXM);