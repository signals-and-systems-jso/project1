clc
clear
close all

% we ask for the constant from user
c=input('please enter a C :');
I = imread('spine.tif');
I2 = im2double(I);
g = c*log(1+I2);
% g = 5*log(1+I2); %or we could use this line for a constant we want
figure();
imshow(I,'Border','tight');
figure();
imshow(g,'Border','tight');

% Linear Test
j = imread('Tulips.jpg');
q = imread('Chrysanthemum.jpg');
j2 = im2double(j);
g2 = 5*log(1+j2);
q2 = im2double(q);
g3 = 5*log(1+q2);
figure();
imshow(g2,'Border','tight');
figure();
imshow(g3,'Border','tight');
