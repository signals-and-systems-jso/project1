clc
clear
close all

t= - 20:.5:20;

% functional definition using not
ddirac = @(x) double(not(x));
x=@(t) 4 * ddirac(2*t) - ddirac(t + 3) + 3 * ddirac(t - 1);
s=@(t) x(2 * t);
p=@(t) x(-t+3);
y=@(t) 2*cos(.45*t) .* exp(-.6*(1i)*t) .* (heaviside(t)- heaviside(t-15));

figure();
subplot(3,3,1);
stem(t,x(t));
title('stem');
subplot(3,3,2);
stem(t,abs(x(t)));
title('stem abs');
subplot(3,3,3);
stem(t,angle(x(t)));
title('stem angle');

subplot(3,3,4);
plot(t,x(t));
title('plot');
subplot(3,3,5);
plot(t,abs(x(t)));
title('plot abs');
subplot(3,3,6);
plot(t,angle(x(t)));
title('plot angle');


figure();
subplot(3,3,1);
stem(t,s(t));
title('stem');
subplot(3,3,2);
stem(t,abs(s(t)));
title('stem abs');
subplot(3,3,3);
stem(t,angle(s(t)));
title('stem angle');

subplot(3,3,4);
plot(t,s(t));
title('plot');
subplot(3,3,5);
plot(t,abs(s(t)));
title('plot abs');
subplot(3,3,6);
plot(t,angle(s(t)));
title('plot angle');


figure();
subplot(3,3,1);
stem(t,p(t));
title('stem');
subplot(3,3,2);
stem(t,abs(p(t)));
title('stem abs');
subplot(3,3,3);
stem(t,angle(p(t)));
title('stem angle');

subplot(3,3,4);
plot(t,p(t));
title('plot');
subplot(3,3,5);
plot(t,abs(p(t)));
title('plot abs');
subplot(3,3,6);
plot(t,angle(p(t)));
title('plot angle');


figure();
z = @(t) imag(y(t));
q = @(t) real(y(t));
subplot(3,4,1);
stem(t,z(t));
title('imag stem');
subplot(3,4,2);
stem(t,q(t));
title('real stem');
subplot(3,4,3);
stem(t,abs(z(t)));
title('imag stem abs');
subplot(3,4,4);
stem(t,abs(q(t)));
title('real stem abs');
subplot(3,4,5);
stem(t,angle(z(t)));
title('imag stem angle');
subplot(3,4,6);
stem(t,angle(q(t)));
title('real stem angle');

subplot(3,4,7);
plot(t,z(t));
title('imag plot');
subplot(3,4,8);
plot(t,q(t));
title('real plot');
subplot(3,4,9);
plot(t,abs(z(t)));
title('imag plot abs');
subplot(3,4,10);
plot(t,abs(q(t)));
title('real plot abs');
subplot(3,4,11);
plot(t,angle(z(t)));
title('imag plot angle');
subplot(3,4,12);
stem(t,angle(q(t)));
title('real plot angle');




% another kind of defining by matlabs dirac
% defining every dirac and map them in infinity to 1
b=dirac(2 * t);
b(b == Inf) = 1;
m=dirac(t + 3);
m(m == Inf) = 1;
n=dirac(t - 1);
n(n == Inf) = 1;
% ----------------------------------------
x=4 * b - m + 3 * n;
% stem(t,x)
% plot(t,x)

% defining every dirac and map them in infinity to 1
b=dirac(4 * t);
b(b == Inf) = 1;
m=dirac(2 * t + 3);
m(m == Inf) = 1;
n=dirac(2 * t - 1);
n(n == Inf) = 1;
% ----------------------------------------
s=4 * b - m + 3 * n;
% stem(t,s)
% plot(t,s)

% defining every dirac and map them in infinity to 1
b=dirac(2 * ((-1) * t + 3));
b(b == Inf) = 1;
m=dirac(((-1) * t + 3) + 3);
m(m == Inf) = 1;
n=dirac(((-1) * t + 3) - 1);
n(n == Inf) = 1;
% ----------------------------------------
p=4 * b - m + 3 * n;
% stem(t,p)
% plot(t,p)

% defining every function 
v=cos((.45) * t);
m=heaviside(t);
n=heaviside(t-15);
f=exp((-1)*(.6)*(1i)*t);
% ----------------------------------------
y=2.*v.*f.*(m-n);
% stem(t,y)
% plot(t,y)

