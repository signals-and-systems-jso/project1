clc
clear
close all

t= - 20:.5:20;

% functional definition
ddirac = @(x) double(not(x));
x=@(t) 4 * ddirac(2*t) - ddirac(t + 3) + 3 * ddirac(t - 1);
s=@(t) x(2 * t);
p=@(t) x(-t+3);
y=@(t) 2*cos(.45*t) .* exp(-.6*(1i)*t) .* (heaviside(t)- heaviside(t-15));
disp('---------------x DC---------------');
a=mean(x(t))
e=sum(x(t))/81
disp('---------------s DC---------------');
b=mean(s(t))
f=sum(s(t))/81
disp('---------------p DC---------------');
c=mean(p(t))
g=sum(p(t))/81
disp('---------------y DC---------------');
d=mean(y(t))
h=sum(y(t))/81



