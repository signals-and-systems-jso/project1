clc
clear
close all

%  fi=conv(x(n) , y(-n)) this is de definition
%  so i will use my conv function and just make second function y(-n)

t= - 10:1:10;

% functional definition using not
ddirac = @(x) double(not(x));
x=@(t) 4 * ddirac(2*t) - ddirac(t + 3) + 3 * ddirac(t - 1);
y=@(t) ((1.1).^(2 * t)) .* (heaviside(t+3)- heaviside(t-2));

figure();
subplot(2,1,1);
% cross corolation is fi=conv(x(n) , y(-n))
CCOR1 = CCOR(x(t) , y(t));
stem(CCOR1);
title('My CCOR');
subplot(2,1,2);
CCOR2 = xcorr(x(t) , y(t));
stem(CCOR2);
title('matlab xcorr');

% to check the result
% figure();
% CCOR1 = conv(x(t) , y(-t));
% stem(CCOR1);