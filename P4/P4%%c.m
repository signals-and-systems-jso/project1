clc
clear
close all

%  fi=conv(x(n) , x(-n)) this is de definition
%  so i will use my conv function and just make second function x(-n)

t= - 10:1:10;

% functional definition using not
ddirac = @(x) double(not(x));
x=@(t) 4 * ddirac(2*t) - ddirac(t + 3) + 3 * ddirac(t - 1);
y=@(t) ((1.1).^(2 * t)) .* (heaviside(t+3)- heaviside(t-2));
z=@(t) sin(t);
v=@(t) 2*cos(.45*t) .* exp(-.6*(1i)*t) .* (heaviside(t)- heaviside(t-15));

% auto cross corolation is fi=conv(x(n) , x(-n))
CCOR1 = CCOR(x(t) , x(t));
stem(CCOR1);
figure();
% auto cross corolation is fi=conv(z(n) , z(-n))
CCOR2 = xcorr(z(t) , z(t));
stem(CCOR2);
% to check the result
% figure();
% CCOR1 = conv(x(t) , y(-t));
% stem(CCOR1);