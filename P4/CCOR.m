function y=CCOR(h,x) 
% first we should find the length of every function 
x2=h; 
lx=length(x); 
lh=length(h); 
% check wich one is bigger then make rest of other one  zero
if lx>lh 
x2=[x2 zeros(1,lx-lh)]; 
else 
x=[x zeros(1,lh-lx)]; 
end 
y=zeros(1,lx+lh-1); 
x=real(x) - 1i*imag(x);
% moving one function and finding sum og every stage for conv 
for i=1:length(y) 
if i<=length(x) 
% it is important to use .* becuase we need to cross one by one
y(i)=sum(x(1,length(x)-i+1:length(x)) .* x2(1,1:i)); 
else 
j=i-length(x); 
y(i)=sum(x(1,1:length(x)-j) .* x2(1,j+1:length(x2))); 
end 
end 